#    guppy Copyright (C) 2010-2011 guppy team members.
#
#    This program comes with ABSOLUTELY NO WARRANTY; for details type `show w'.
#    This is free software, and you are welcome to redistribute it
#    under certain conditions; type `show c' for details.
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.

import time
#import datetime
#from datetime import datetime as dt
import os
import sqlite3
import threading
import urllib.request
import xml.etree.ElementTree as ET


@plugin
class Rss(object):
    def __init__(self, server):
        # Variables.
        self.server = server
        self.network = self.server.config["network"]
        self.commands = ["rss-add", "rss-del", "rss-list", "rss-check-feeds"]
        self.loop = True
        self.server.handle("command", self.handle_command, self.commands)
        self.network = self.server.config["network"]
        # Define and/or create database.
        self.db = os.path.join(self.server.config["confdir"], "rss.db")
        self.db_exists = os.path.isfile(self.db)
        self.connection = sqlite3.connect(self.db)
        self.cursor = self.connection.cursor()
        if not self.db_exists:
            # create the db
            self.cursor.execute('''CREATE TABLE rss (id text, network text, channel text, url text)''')
            self.connection.commit()
            print("[DEBUG] [rss] Database created for the first time")
        # Set up the thread to check for new feeds.
        self.timeCheckFeeds = 60

        def loop(plugin, server):
            timeWaited = 0
            while self.loop:
                if timeWaited >= self.timeCheckFeeds:
                    self.checkFeeds()
                    timeWaited = 0
                time.sleep(10)
                timeWaited += 10
        self.t1 = threading.Thread(target=loop, args=(self, server,))
        self.t1.daemon = True
        self.t1.start()

    def handle_command(self, channel, user, cmd, args):
        connection = sqlite3.connect(self.db)
        cursor = connection.cursor()
        if cmd == 'rss-add':
            url = args[0]
            cursor.execute('''SELECT url FROM rss WHERE network = ? and channel = ? and url = ?''', (self.network, channel, url))
            if len(cursor.fetchall()) == 0:
                cursor.execute('''INSERT INTO rss VALUES(?,?,?,?)''', ('', self.network, channel, url))
                connection.commit()
                self.server.doMessage(channel, "Done. RSS item %s added to %s." % (url, channel))
            else:
                self.server.doMessage(channel, "Error: RSS item %s already exists at %s" % (url, channel))
        elif cmd == 'rss-list':
            cursor.execute('''SELECT url FROM rss WHERE network = ? and channel = ?''', (self.network, channel))
            for my_url in cursor.fetchall():
                self.server.doMessage(channel, "RSS item: " + my_url[0])
        elif cmd == 'rss-del':
            url = args[0]
            cursor.execute('''SELECT url FROM rss WHERE network = ? and channel = ? and url = ?''', (self.network, channel, url))
            if len(cursor.fetchall()) == 0:
                self.server.doMessage(channel, "Error: %s RSS feed not found" % url)
            else:
                try:
                    cursor.execute('''DELETE FROM rss WHERE network = ? and channel = ? and url = ?''', (self.network, channel, url))
                    self.server.doMessage(channel, url + " RSS feed removed")
                    connection.commit()
                except sqlite3.Error as e:
                    self.server.doMessage(channel, "Error: %s RSS feed not removed (%s)" % (args[0], e.args[0]))
        elif cmd == 'rss-check-feeds':
            self.checkFeeds()
        cursor.close()
        connection.close()

    def announceFeed(self, row):
        self.server.prnt("Announcing feed... " + str(row))
        # row = ('1', 'gnu', '#guppy', 'http://phys.org/rss-feed/')
        url = row[3]
        last_known_id = row[0]
        bool_updated = 0
        request = urllib.request.Request(url, headers={'user-agent': 'guppy ' + self.server.config["version"]})
        s = urllib.request.urlopen(request)
        s = s.read()
        root = ET.fromstring(s)
        for channel in root:
            children = channel.findall('item')
            for child in children:
                feed_id = child.find('guid').text
                if feed_id == last_known_id:
                    return
                if bool_updated == 0:
                    # update the db
                    connection = sqlite3.connect(self.db)
                    cursor = connection.cursor()
                    cursor.execute("UPDATE rss SET id = ? where network = ? and channel = ? and url = ?", (feed_id, row[1], row[2], row[3]))
                    connection.commit()
                    connection.close()
                    bool_updated = 1
                title = child.find('title').text
                url = child.find('link').text
                url = urllib.request.urlopen("http://tinyurl.com/api-create.php?url=" + url).readline().decode('utf8')
                msg = url + " "
                if child.find('category') is not None:
                    msg += "[" + child.find('category').text + "]" + " "
                msg += title
                self.server.doMessage(row[2], msg)
                # print("%s [%s] %s" % (str(url), str(cat), str(title)))

    def checkFeeds(self):
        connection = sqlite3.connect(self.db)
        cursor = connection.cursor()
        cursor.execute('SELECT * FROM rss')
        for row in cursor.fetchall():
            self.announceFeed(row)
        cursor.close()
