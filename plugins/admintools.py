#    guppy Copyright (C) 2010-2011 guppy team members.
#
#    This program comes with ABSOLUTELY NO WARRANTY; for details type `show w'.
#    This is free software, and you are welcome to redistribute it
#    under certain conditions; type `show c' for details.
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.


@plugin
class AdminTools(object):
    """admin-only commands, channel actions, nick, attrs (debug info)"""
    def __init__(self, server):
        self.server = server
        self.server.pluginManager.loadPlugin("auth")
        self.commands = ["nick", "join", "part", "kick", "ban", "mute", "unban", "unmute", "op", "deop", "voice", "devoice", "unop", "unvoice", "attrs"]
        self.server.handle("command", self.handle_command, self.commands)

    def handle_command(self, channel, user, cmd, args):
        if self.server.getPlugin("auth").isAdmin(user):
            if cmd == "op":
                if len(args) < 1:
                    self.server.doMode(channel, "+o", user)
                else:
                    self.server.doMode(channel, "+o", args[0])
            elif cmd == "deop" or cmd == "unop":
                if len(args) < 1:
                    self.server.doMode(channel, "-o", user)
                else:
                    self.server.doMode(channel, "-o", args[0])
            elif cmd == "voice":
                if len(args) < 1:
                    self.server.doMode(channel, "+v", user)
                else:
                    self.server.doMode(channel, "+v", args[0])
            elif cmd == "devoice" or cmd == "unvoice":
                if len(args) < 1:
                    self.server.doMode(channel, "-v", user)
                else:
                    self.server.doMode(channel, "-v", args[0])
            else:

                if len(args) < 1:
                    self.server.doMessage(channel, user + ": Not enough arguments.")
                    return

                if cmd == "join":
                    self.server.doJoin(args[0])
                elif cmd == "part":
                    self.server.doPart(args[0])
                elif cmd == "nick":
                    self.server.doNick(args[0])
                elif self.server.config["nickname"] not in args[0]:
                    if cmd == "ban":
                        self.server.doMode(channel, "+b", args[0])
                        self.server.doKick(channel, args[0], "Banned!")
                    elif cmd == "unban":
                        self.server.doMode(channel, "-b", args[0])
                    elif cmd == "mute":
                        self.server.doMode(channel, "+q", args[0])
                    elif cmd == "unmute":
                        self.server.doMode(channel, "-q", args[0])
                    elif cmd == "kick":
                        self.server.doKick(channel, args[0], "Kicked!")

        if self.server.getPlugin("auth").isOwner(user):
            if cmd == "die":
                self.server.doMessage(channel, user + " wants me to leave, but I'll be back!")
                self.server.doQuit()
            elif cmd == "attrs":  # attrs. --Kudu.
                if len(args) == 1:
                    self.server.doMessage(channel, user + ": " + str(dir(self.server.getPlugin(args[0]))))
                elif len(args) == 2:
                    self.server.doMessage(channel, user + ": " + str(getattr(self.server.getPlugin(args[0]), args[1])))
                elif len(args) == 3:
                    setattr(self.server.getPlugin(args[0]), args[1], args[2])
