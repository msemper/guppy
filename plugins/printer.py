#    guppy Copyright (C) 2010-2011 guppy team members.
#
#    This program comes with ABSOLUTELY NO WARRANTY; for details type `show w'.
#    This is free software, and you are welcome to redistribute it
#    under certain conditions; type `show c' for details.
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.


@plugin
class Printer(object):
    def __init__(self, server):
        self.server = server
        self.commands = []
        self.prnt = server.prnt

        self.server.handle("message", self.handle_message)
        self.server.handle("kick", self.handle_kick)
        self.server.handle("part", self.handle_part)
        self.server.handle("join", self.handle_join)
        self.server.handle("quit", self.handle_quit)
        self.server.handle("mode", self.handle_mode)
        self.server.handle("notice", self.handle_notice)
        self.server.handle("connect", self.handle_connect)
        self.server.handle("disconnect", self.handle_disconnect)
        self.server.handle("action", self.handle_action)
        self.server.handle("nick", self.handle_nick)
        self.server.handle("error", self.handle_error)

    def handle_connect(self, server):
        self.prnt("I have connected to " + server)

    def handle_disconnect(self, server):
        self.prnt("I have disconnected from " + server)

    def handle_message(self, channel, user, message):
        self.prnt("<%s> %s: %s" % (channel, user, message))

    def handle_action(self, channel, user, action):
        self.prnt("<%s> * %s %s" % (channel, user, action))

    def handle_join(self, channel, user):
        self.prnt("%s has joined %s" % (user, channel))

    def handle_part(self, channel, user, message):
        self.prnt("%s has left %s (%s)" % (user, channel, message))

    def handle_error(self, message):
        self.prnt("ERROR: " + message)

    def handle_quit(self, user, message):
        self.prnt("%s has quit (%s)" % (user, message))

    def handle_nick(self, oldnick, newnick):
        self.prnt("%s is now known as %s" % (oldnick, newnick))

    def handle_kick(self, channel, user, userkicked, message):
        self.prnt("%s has kicked %s from %s (%s)" % (user, userkicked, channel, message))

    def handle_notice(self, user, dest, message):
        self.prnt("(%s) -%s- %s" % (dest, user, message))

    def handle_mode(self, channel, user, mode, otheruser):
        self.prnt("%s set mode %s on %s" % (user, mode, otheruser if otheruser != "" else channel))
