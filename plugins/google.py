#    guppy Copyright (C) 2010-2012 guppy team members.
#
#    This program comes with ABSOLUTELY NO WARRANTY; for details type `show w'.
#    This is free software, and you are welcome to redistribute it
#    under certain conditions; type `show c' for details.
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.

# Google
# ~ Nathan Hakak (aka DrKabob)

import urllib.request, urllib.parse
import json

@plugin
class Google(object):
    """Searches the web with Google. Google is an extremely popular search engine."""
    def __init__(self, server):
        self.commands = ["google", "g"]
        self.server = server
        self.server.handle("command", self.handle_command, self.commands)

    def handle_command(self, channel, user, cmd, args):
        if cmd == "google" or cmd == "g":
            search = " ".join(args)
            req = urllib.request.Request("http://ajax.googleapis.com/ajax/services/search/web?v=1.0&q=" + urllib.parse.quote(search) + "&key=ABQIAAAA4B16PYoznYWgfVLfNDV4fxRsamdul3hUHNYXnxki2eGK76NS_RQ795CTZZ3l-TuRCO2d5eibFI1WZA")
            data = urllib.request.urlopen(req).read()
            data = json.loads(str(data,encoding='utf-8'))
            results = data['responseData']['results']
            if(len(results)>0):
                result1=results[0]
                self.server.doMessage(channel, result1['titleNoFormatting'] + ' - ' + result1['unescapedUrl'])
