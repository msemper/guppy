#    guppy Copyright (C) 2010-2011 guppy team members.
#
#    This program comes with ABSOLUTELY NO WARRANTY; for details type `show w'.
#    This is free software, and you are welcome to redistribute it
#    under certain conditions; type `show c' for details.
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.

# ctcp module
# --gry

import time


@plugin
class ctcp(object):
    """Add support for CTCP source, CTCP version."""
    def __init__(self, server):
        self.prnt = server.prnt
        self.server = server
        self.commands = []
        self.server.handle("message", self.handle_message)

    def handle_message(self, channel, nick, message):
        if message.find("\001") == -1:
            return
        message_array = message.replace("\001", "", 2).strip().lower().split()
        ctcp_type = message_array[0]
        if len(message_array) > 1:
            ctcp_args = " ".join(message_array[1:])
        else:
            ctcp_args = ""
        self.prnt("CTCP typ: " + ctcp_type + " came in")
        self.prnt("CTCP args: " + ctcp_args + " came in")
        if ctcp_type == "version":
            self.server.doNotice(nick, "\001VERSION guppy " + self.server.config["version"] + "\001")
        elif ctcp_type == "clientinfo":
            self.server.doNotice(nick, "\001VERSION guppy " + self.server.config["version"] + "\001")
        elif ctcp_type == "source":
            self.server.doNotice(nick, "\001VERSION http://repo.or.cz/w/guppy.git\001")
        elif ctcp_type == "ping":
            self.server.doNotice(nick, "\001PING " + ctcp_args + "\001")
        elif ctcp_type == "time":
            self.server.doNotice(nick, "\001TIME " + time.strftime("%a %b %d %H:%M:%S %Y %z") + "\001")
