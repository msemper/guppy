#import urllib
import urllib.request
import html.parser

@plugin
class Urltitle(object):
    # The class name is the plugin name, case insensitive
    """ Url title plugin """
    def __init__(self, server):
        self.server = server
        self.prnt = server.prnt
        self.nick = self.server.config["nickname"]
        self.commands = ["urltitle"]
        self.parse_url_titles_in_message = True
        self.server.handle("command", self.handle_command, self.commands)
        server.handle("message", self.handle_message)

    def handle_command(self, channel, user, cmd, args):
        if cmd == "urltitle" and user != self.nick:
            message = self._get_title(args[0])
            if message == None:
                return
            self.server.doMessage(channel, message)

    def url_okay(self, url):
        if url in self.urls:
            return 0

    def _get_title(self, url):
        try:
            request = urllib.request.Request(url, headers={'user-agent': 'guppy ' + self.server.config["version"]})
            s = urllib.request.urlopen(request)
            title = s.read().decode('utf-8', 'replace').split("<title>")[1].split("</title>")[0]
            title = html.parser.HTMLParser().unescape(title)
            return url + " - " + title
        except:
            return None

    def handle_message(self, channel, user, message):
        if self.parse_url_titles_in_message and message.find('urltitle') == -1 and user != self.nick:
            all_urls = [url for url in message.split() if url.startswith("http://") or url.startswith("https://")]
            results = {self._get_title(url) for url in all_urls}  # {}s are a set; only unique members
            # pyflakes complains that the above is invalid syntax, but it isn't. -- auscompgeek
            for url in results:
                if url is not None:
                    self.server.doMessage(channel, url)
