#    guppy Copyright (C) 2010-2011 guppy team members.
#
#    This program comes with ABSOLUTELY NO WARRANTY; for details type `show w'.
#    This is free software, and you are welcome to redistribute it
#    under certain conditions; type `show c' for details.
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.


# We need the decorator, otherwise the
# bot doesn't see it's a plugin
@plugin
class Printer(object):
    # The class name is the plugin name, case insensitive
    """ Replace this comment with a help line for the plugin """
    def __init__(self, server):
        """
        server is an instance of class IRC,
        you'll use it to do anything with the server,
        for example sending a message
        """
        self.server = server

        """
        You must use this to provide any helpful output,
        it automatically adds the [host] prefix for you
        """
        self.prnt = server.prnt

        """
        This is the array of commands that this plugin will handle.
        It doesn't need to be defined, it is passed as an argument to
        self.server.handle("command", function, ["command1", "command2"])
        """
        self.commands = ["ping"]

        """
        this is just so that we don't have to hard-code every event
        (self.server.handlers has every type of event handle-able)
        but you can do it like "self.server.handle("event", self.function)"
        """
        for h in self.server.handlers:
            func = getattr(self, "handle_" + h, None)
            if func is not None:
                if h == "command":
                    self.server.handle(h, func, self.commands)
                else:
                    self.server.handle(h, func)

        """
        any of the following functions will only be called
        if you set them up using server.handle(....)
        keep in mind, these function names are for example,
        you can use any names you'd like.

        Also, all arguments named "user" is actually a subclass of str,
        so that used normally, it provides the nickname, but you can also use
        the member fields user.ident and user.host
        """

    def handle_command(self, channel, user, cmd, args):
        """
        Called when we get a command
        It will be called when we are addressed, as well,
        thus allowing "Guppy: <command>" without the
        comchar prefix.
        """
        #in case we are handling a lot of commands
        if cmd == "ping":
            #send a response
            self.server.doMessage(channel, user[0] + ": Pong")

    def handle_connect(self, server):
        """ Called when we connect to the server """
        self.prnt("I have connected to " + server)

    def handle_disconnect(self, server):
        """ Called when we disconnect from the server """
        self.prnt("I have disconnected from " + server)

    def handle_message(self, channel, user, message):
        """ Called when a message is received """
        self.prnt("<%s> %s: %s" % (channel, user[0], message))

    def handle_action(self, channel, user, action):
        """ Called when we see someone doing an action """
        self.prnt("<%s> * %s %s" % (channel, user[0], action))

    def handle_join(self, channel, user):
        """ Called when we see someone join a channel we're in """
        self.prnt("%s has joined %s" % (user[0], channel))

    def handle_part(self, channel, user, message):
        """
        Called when someone parts a channel we're in
        param message will be "" if there was no part message
        """
        self.prnt("%s has left %s (%s)" % (user[0], channel, message))

    def handle_quit(self, user, message):
        """
        Called when someone quits IRC
        param message will be "" if there was no quit message
        """
        self.prnt("%s has quit (%s)" % (user[0], message))

    def handle_nick(self, oldnick, newnick):
        """ Called when someone changes their nick """
        self.prnt("%s is now known as %s" % (oldnick, newnick))

    def handle_kick(self, channel, user, userkicked, message):
        """ Called when someone is kicked from a channel we're in """
        self.prnt("%s has kicked %s from %s (%s)" % (user, userkicked, channel, message))

    def handle_notice(self, user, dest, message):
        """ Called when a notice is received """
        self.prnt("(%s) -%s- %s" % (dest, user, message))

    def handle_mode(self, channel, user, mode, otheruser):
        """
        Called when someone sets a mode
        param otheruser will be "" if the mode was set
        onto the channel, otherwise it will be the nickname
        of the user who had the mode set upon them
        """
        self.prnt("%s set mode %s on %s" % (user[0], mode, otheruser if otheruser != "" else channel))
